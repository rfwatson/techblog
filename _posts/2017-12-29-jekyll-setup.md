---
layout: post
title:  "Notes from setting up a Jekyll blog on GitLab"
date:   2017-12-29 15:00:00 +0000
categories: jekyll setup
---

Here are my notes from today's task of setting up this blog.

* Using [Jekyll](https://jekyllrb.com/)
  * Easy to setup
  * Built-in syntax highlighting
  * write/commit/push workflow
  * Compatible with GitHub and GitLab pages
* Theme
  * Using [minima theme](https://github.com/jekyll/minima)
  * Basic but easy to customize and extend
  * Overrides:
    * [`home.html`](https://gitlab.com/rfwatson/techblog/commit/1f9e5bcc69b1d25329c7552a7d3152ff63725250#f87bb2fcce406e16253570011cce02227981f242_0_25) to add full post content to homepage
    * [`assets/main.scss`](https://gitlab.com/rfwatson/techblog/blob/1f9e5bcc69b1d25329c7552a7d3152ff63725250/assets/main.scss) with CSS/design changes
  * Fonts
    * Added free webfonts from [Google Fonts](https://fonts.google.com/)
* Hosting
  * Using [GitLab pages](https://about.gitlab.com/features/pages/)
  * Free static site hosting similar to GitHub Pages
  * GitLab Pages pros/cons:
    * HTTPS support for static site, even on custom domains
    * No option to force HTTPS
    * Powerful containerized [CI/build system](https://about.gitlab.com/features/gitlab-ci-cd/)
  * GitHub Pages pros/cons:
    * HTTPS support, including option to force HTTPS
    * No HTTPS support for custom domains
  * Added [`gitlab-ci.yml`](https://gitlab.com/rfwatson/techblog/blob/master/.gitlab-ci.yml) to build Jekyll site after each push
* SSL support
  * Used [LetsEncrypt](https://letsencrypt.org/)
  * Followed [this tutorial](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/)
    * *but* must add `/` to end of `permalink` YAML entry to avoid 404 response from GitLab
  * Avoid using personal internet connection when following above tutorial, as IP address is publicly logged by LetsEncrypt. Run letsencrypt from server if possible.
* DNS
  * Domain registered with [Namecheap](https://www.namecheap.com)
  * Added hosted zone to [AWS Route 53](https://aws.amazon.com/route53/)
  * Updated nameserver records in Namecheap control panel to point at AWS
  * Added A record to point at GitLab pages IP address `52.167.214.135`
  * Added MX record to point at pre-existing [Mailinabox](http://mailinabox.email/) setup
