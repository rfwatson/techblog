---
layout: page
title: About
permalink: /about/
---

<img src="../assets/images/me.png" alt="Rob Watson" class="profile"/>

Hi, I'm Rob Watson and this is my tech/coding blog.

I co-founded live audio broadcasting company [Mixlr](http://mixlr.com), and have served as its CTO since 2011.

I also have a degree in [Music Informatics](https://en.wikipedia.org/wiki/Music_informatics) and have a long-standing interest in musical composition and performance using computers.

You can browse my [GitHub](https://github.com/rfwatson) or download my [professional resume](https://s3.eu-central-1.amazonaws.com/rfwatson-public/cv.pdf). You can also find my personal blog [here](https://www.rfwatson.net).
